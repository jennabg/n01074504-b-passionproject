namespace PassionProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FoodPhotoes", "ImagePath", c => c.String(maxLength: 150));
            DropColumn("dbo.FoodPhotoes", "HasPic");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FoodPhotoes", "HasPic", c => c.Int(nullable: false));
            DropColumn("dbo.FoodPhotoes", "ImagePath");
        }
    }
}
