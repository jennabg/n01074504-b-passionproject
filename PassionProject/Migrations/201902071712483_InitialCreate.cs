namespace PassionProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FoodPhotoes",
                c => new
                    {
                        FoodPhotoID = c.Int(nullable: false, identity: true),
                        PhotoCaption = c.String(),
                        HasPic = c.Int(nullable: false),
                        PhotoDetails = c.String(),
                        user_UserID = c.Int(),
                    })
                .PrimaryKey(t => t.FoodPhotoID)
                .ForeignKey("dbo.Users", t => t.user_UserID)
                .Index(t => t.user_UserID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FoodPhotoes", "user_UserID", "dbo.Users");
            DropIndex("dbo.FoodPhotoes", new[] { "user_UserID" });
            DropTable("dbo.Users");
            DropTable("dbo.FoodPhotoes");
        }
    }
}
