﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;

namespace PassionProject.Controllers
{
    public class UserController : Controller
    {
        private FoodPhotoCMSContext db = new FoodPhotoCMSContext();
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
   

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string UserName_New, User user)
        {
            string query = "insert into Users(UserName)" + "values(@username)";
            SqlParameter[] myparams = {
                 new SqlParameter("@username", UserName_New)
            };
            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");
        }

    public ActionResult List()
        {
            string query = "select * from Users";
            IEnumerable<User> users = db.Users.SqlQuery(query);

            return View(users);
        }

    public ActionResult Delete(int id)
        {

            string query = "delete from FoodPhotoes where User_UserID=@id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            query = "delete from Users where UserID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

           


            return RedirectToAction("List");
        }

    public ActionResult Show(int? id)
        {

            // checking to make the object we want to show exists
            if ((id == null) || (db.Users.Find(id) == null))
            {
                return HttpNotFound();
            }

            // query to display a single object from a database where object id matches passed id 
            string query = "select * from Users where UserID=@id";
            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@id", id);

            User newuser = db.Users.SqlQuery(query, myparams).FirstOrDefault();

            return View(newuser);
        }

        /* couldn't get this to work so left it in to show what I tried but commented it out and
         * deleted the view so the files would still run
         * public ActionResult Edit(int? id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(int id, string UserName, User user)
        {
            // checking to make the object we are editing exists
            if ((id == null) || (db.Users.Find(id) == null))
            {
                return HttpNotFound();
            }

            // Query to update exisiting object and it's fields in the database
            string query = "update Users set UserName=@username where userid=@id";
            SqlParameter[] myparams = {
                 new SqlParameter("@username", UserName),
                 new SqlParameter("@id",id)

            };

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }*/


    }
}