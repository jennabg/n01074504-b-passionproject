﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;
using PassionProject.Models.ViewModels;
using System.Diagnostics;

namespace PassionProject.Controllers
{
    public class FoodPhotoController : Controller
    {
        private FoodPhotoCMSContext db = new FoodPhotoCMSContext();
        // GET: FoodPhoto
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            // Creates multiples of the foodphoto object 
            IEnumerable<FoodPhoto> foodphotos = db.FoodPhotos.ToList();
            return View(foodphotos);
        }

        public ActionResult Create()
        {
            //Creates multiple user objects and puts them into a list
            IEnumerable<User> usernames = db.Users.ToList();
                return View(usernames);
        }
        // This code mainly references Christine's project however Kento helped me with the image uploading feature
        // Here are the links to the tutorials used https://stackoverflow.com/questions/16255882/uploading-displaying-images-in-mvc-4
        // https://stackoverflow.com/questions/24625078/how-generate-a-unique-file-name-at-upload-files-in-webserver-mvc
        [HttpPost]
        public ActionResult Create(string PhotoCaption_New, string PhotoDetails_New, int UserUploader_New, FoodPhoto foodphoto, HttpPostedFileBase file)
        {
            if (file != null)
                {

                // Saving the image with a unique name
                var newImg = DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);

                //Save the actual root image file
                file.SaveAs(HttpContext.Server.MapPath("~/Images/") + newImg);

                foodphoto.ImagePath = newImg;

                string query = "insert into FoodPhotoes (ImagePath, PhotoCaption, PhotoDetails, User_UserID) values(@ImagePath, @caption, @details, @user)";
                SqlParameter[] myparams = {
                     new SqlParameter("@ImagePath", foodphoto.ImagePath),
                     new SqlParameter("@caption", PhotoCaption_New),
                     new SqlParameter("@details", PhotoDetails_New),
                     new SqlParameter("@user", UserUploader_New)

                };

                db.Database.ExecuteSqlCommand(query, myparams);
                db.SaveChanges();
            }
            return RedirectToAction("List");
        }

        public ActionResult Edit(int? id)
        {
            // Edit action -> uses the foodphoto edit made to grab a single foodphoto object and multiple users to be displayed on the
            //edit view page
            FoodPhotoEdit foodphotoeditview = new FoodPhotoEdit();
            foodphotoeditview.users = db.Users.ToList();
            foodphotoeditview.foodphoto = db.FoodPhotos.Find(id);

            return View(foodphotoeditview);
        }
        [HttpPost]
        public ActionResult Edit(int? id, string PhotoCaption, string PhotoDetails, int Username, FoodPhoto foodphoto)
        {
            // checking to make the object we are editing exists
            if ((id== null) || (db.FoodPhotos.Find(id)== null))
            {
                return HttpNotFound();
            }

           // Query to update exisiting object and it's fields in the database
            string query = "update FoodPhotoes set PhotoCaption = @caption, PhotoDetails=@details, User_UserID=@user where foodphotoid=@id";
            SqlParameter[] myparams = {
                 new SqlParameter("@caption", PhotoCaption),
                 new SqlParameter("@details", PhotoDetails),
                 new SqlParameter("@user", Username),
                 new SqlParameter("@id",id)

            };

              db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        public ActionResult Delete(int id)
        {
           //query to delete an object from the database where the object matches the id passed through
            string query = "delete from FoodPhotoes where FoodPhotoID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));


            return RedirectToAction("List");
        }

        public ActionResult Show(int id)
        {

            // checking to make the object we want to show exists
            if ((id == null) || (db.FoodPhotos.Find(id) == null))
            {
                return HttpNotFound();
            }

            // query to display a single object from a database where object id matches passed id 
            string query = "select * from FoodPhotoes where FoodPhotoID=@id";
            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@id", id);

            FoodPhoto newfoodphoto = db.FoodPhotos.SqlQuery(query, myparams).FirstOrDefault();

            return View(newfoodphoto);
        }
    }
}