﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
namespace PassionProject.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }

        [Required,Display(Name ="Username")]
        public string UserName { get; set; }

        public virtual ICollection<FoodPhoto> FoodPhotos { get; set; }
    }
}