﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class FoodPhoto
    {
            [Key]
             public int FoodPhotoID { get; set; }


            // Column to save the image location
            [StringLength(150), Display(Name = "Image Path")]
            public string ImagePath { get; set; }

            // Field for the caption
            
            public string PhotoCaption { get; set; }

            //Field for the details

            public string PhotoDetails { get; set; }

            
            //Key to link user to the photo
            public virtual User user { get; set; }
            
        
    }
}