﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PassionProject.Models
{
    public class FoodPhotoCMSContext : DbContext
    {
        public FoodPhotoCMSContext()
        {

        }

        public DbSet<FoodPhoto> FoodPhotos { get; set; }
        public DbSet<User> Users { get; set; }
    }
}